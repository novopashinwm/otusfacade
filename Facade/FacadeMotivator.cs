﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Text;

namespace Facade
{
	public class FacadeMotivator
	{
		private Image img;
		
		public string CreateMotivator(String image, String message)
		{
			SetImageFile(image);
			DrawText(message);
			ImageSave();
		    return string.Concat(AppDomain.CurrentDomain.BaseDirectory, GetResultFile());
		}

		private string GetFullResultFile() 
		{ 
			return string.Concat(AppDomain.CurrentDomain.BaseDirectory, GetResultFile());
		}

		private void SetImageFile(string image) 
		{
			img = Image.FromFile(image);
		}

		private void DrawText(string message)
		{
			using (Graphics graph = Graphics.FromImage(img))
			{
				Font font = new Font("Tahoma", 24);
				graph.DrawString(message, font, Brushes.White, 60, 520);
			}
		}

		private void ImageSave() 
		{
			img.Save(GetResultFile(), ImageFormat.Jpeg);
		}

		private string GetResultFile() 
		{
			return "resutl.jpg";
		}

	}
}
