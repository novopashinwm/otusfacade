﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Facade
{
    class Program
    {
        static void Main(string[] args)
        {
            FacadeMotivator facade = new FacadeMotivator();
            Process.Start(facade.CreateMotivator("motivator.jpg", "Не останавливайся!!!"));
        }
    }
}
